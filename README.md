# Prueba técnica Cidenet #

La aplicación fue desarrollada con el lenguaje de programación PHP, el framework Laravel, la libreria ReactJS y una base de datos MySQL. También se utilizo composer y npm

## Instalación ##
*   Primero clonar este repositorio en la carpeta donde desee guardar los archivos: 
```
git clone https://gitlab.com/Neo1277/prueba-tecnica-cidenet.git
``` 
*   Instalar las dependencias de Laravel con composer, debes estar situado dentro de la carpeta backend_laravel:. 
```
composer install
``` 
*   Por seguridad el archivo .env está excluido del repositorio, para generar uno nuevo se toma como plantilla el archivo .env.example para copiar este archivo en una nuevo escribe en tu terminal (si usas Windows usa Git Bash):
```
cp .env.example .env
``` 
*   Ahora se debe generar una key de seguridad para el proyecto:
```
php artisan key:generate
``` 
*   Despues iniciar la base de datos MySQL. En este caso yo utilice apache y ahi inicie la base de datos MySQL.
*   Ahora para conectar la aplicacion de Laravel con la base de datos se debe crear una base de datos y poner el nombre de la base de datos en el archivo .env y establecer el valor de la variable DB_DATABASE con el de la base de datos creada para este proyecto, el archivo .env se encuentra en: 
```
backend_laravel\.env
``` 
*   Tener en cuenta que si se tienen credenciales para acceder a la base de datos diferentes, estos deben cambiarse en las variables que estan en el archivo .env
*   Para migrar las tablas de los modelos a la base de datos, debes estar situado dentro de la carpeta backend_laravel y ahi ejecutar el siguiente comando:
```
php artisan migrate
``` 
*   Para enviar algunos registros a la base de datos desde Laravel (registros de paises, areas, tipos de documento), estos seeders fueron creados por mi y se ejecutan con el siguiente comando
```
php artisan db:seed
``` 
*   Para iniciar aplicacion Laravel ejecutar siguiente comando:
```
php artisan serve
``` 
*   Con esto la parte del lado del servidor ya esta instalada.
*   Para instalar la parte del lado del cliente se debe ir al siguiente directorio:
```
frontend_reactjs
``` 
*   En este directorio se debe ejecutar el siguiente comando en la consola para instalar las dependencias de reactjs con npm:
```
npm install
``` 
*   Para iniciar la aplicación se debe ejecutar el siguiente comando:
```
npm start
``` 
*   El servidor del lado del cliente se ejecutara en la siguiente url:
```
http://localhost:3000/home
```

## Instrucciones de uso ##

*   En la pagina principal aparecera la tabla con los registros, un formulario para hacer busquedas, paginación, adicionar empleados, editar y eliminar con sus respectivas validaciones.


## Ejecutar pruebas unitarias ##
*   para ejecutar las pruebas unitarias del backend Laravel, debes estar situado en la carpeta backend_laravel y ejecutar en Linux:
```
./vendor/bin/phpunit
```
*   Ejecutar pruebas unitarias del backend Laravel en windows:
```
.\vendor\bin\phpunit
```