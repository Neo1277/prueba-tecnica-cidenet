<?php

namespace App\Utilities;

class CorreoElectronicoTercero{

    private $dominio = "cidenet.com";
    
    public function generarCorreoElectronicoSecuencial($cantidad_correos, $request){

        $primer_nombre = preg_replace('/\s+/', '', $request->primer_nombre);
        $primer_apellido = preg_replace('/\s+/', '', $request->primer_apellido);

        $correo_electronico = strtolower($primer_nombre)."."
                            .strtolower($primer_apellido);

        if($cantidad_correos){

            $correo_electronico .= $this->generarIdCorreoElectronico($cantidad_correos->correo_electronico);
            
        }
        
        $correo_electronico .= "@".$this->dominio.".".$request->abreviacion;

        return $correo_electronico;
    }

    
    public function generarIdCorreoElectronico($correo_electronico){
        /**
         * Si ya existe uno o mas  correos con el mismo nombre y apellido
         * extraer el id si ya existe e incrementarlo para
         * generar un id para el nuevo correo electronico
         */
        $extraer_correo = explode("@", $correo_electronico);
        $extraer_id = explode(".", $extraer_correo[0]);

        if (array_key_exists(2, $extraer_id)) {
            $nuevo_id = (int)$extraer_id[2] + 1;
            $nuevo_id = '.'.$nuevo_id;
        }else{
            $nuevo_id = 1;
            $nuevo_id = '.'.$nuevo_id;
        }

        return $nuevo_id;
    }
}