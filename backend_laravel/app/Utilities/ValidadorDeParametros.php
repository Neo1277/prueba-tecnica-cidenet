<?php

namespace App\Utilities;
use Validator;

class ValidadorDeParametros{

    private $tipos_de_busqueda = array(
        "documento_identidad",
        "tipos_de_documento_identidad",
        "primer_apellido",
        "segundo_apellido",
        "primer_nombre",
        "otros_nombres",
        "pais_del_empleo",
        "correo_electronico",
        "estado"
    );

    public function validarParametrosDeBusqueda($filtrar_por=null, $busqueda=null){
        if (empty($filtrar_por) || empty($filtrar_por)) {
            return false;
        }

        return in_array($filtrar_por, $this->tipos_de_busqueda, true);
    }

}

