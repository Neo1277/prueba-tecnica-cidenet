<?php

namespace App\Utilities;
use Validator;
use App\Models\Terceros;
use App\Models\Empleados;
use Illuminate\Support\Facades\Log;

class ConsultasEmpleado{

    public function consultarSiYaExisteDocumentoIdentidadYTipoDeDocumento(array $condicion){
        /**
         * Hacer query en la base de datos para validar si 
         * Ya existe un registro con el mismo tipo de documentod de 
         * identidad y numero de documento identidad
         */
        try{
            $validar_documento_identidad = Empleados::select('terceros.documento_identidad as documento_identidad', 
                                'terceros.id as id_tercero')
                    ->join('terceros', 'empleados.id_tercero', '=', 'terceros.id')
                    ->where($condicion)
                    ->first();       
        } catch (Exception $e) {
            Log::error('Query terceros error: '.$e->getMessage());
            //return response()->json(['error' => $e->getMessage()], 500);
        }
        return $validar_documento_identidad;
    }

    public function consultarSiYaExisteCorreoElectronico($condicion){
        try{
            $cantidad_correos = Terceros::select('correo_electronico')
            ->where($condicion)
            ->orderBy('id', 'DESC')->first();
        } catch (Exception $e) {
            Log::error('Query terceros error: '.$e->getMessage());
            //return response()->json(['error' => $e->getMessage()], 500);
        }

        return $cantidad_correos;
    }

}