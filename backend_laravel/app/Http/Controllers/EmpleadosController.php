<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Terceros;
use App\Models\Empleados;
use Validator;
use Illuminate\Support\Facades\Log;
use App\Utilities\ValidadorDeParametros;
use App\Utilities\ConsultasEmpleado;
use App\Utilities\CorreoElectronicoTercero;
use App\Http\Requests\EmployeesStoreRequest;

class EmpleadosController extends Controller
{

    private $validador_de_parametros;
    private $consultas_empleado;
    private $correo_electronico_tercero;

    private $mensaje_error_documento_identidad = 'Ya existe un registro con el mismo tipo de documentod de '.
                                                    'identidad y numero de documento identidad';

    public function __construct()
    {

        $this->validador_de_parametros = new ValidadorDeParametros();
        $this->consultas_empleado = new ConsultasEmpleado();
        $this->correo_electronico_tercero = new CorreoElectronicoTercero();
    }    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $validacion_de_parametros_de_busqueda = $this->validador_de_parametros->validarParametrosDeBusqueda($request->filtrar_por, $request->busqueda);
        
        try {
            $empleados = Empleados::select('terceros.documento_identidad as documento_identidad', 
                        'terceros.id as id_tercero','terceros.primer_apellido as primer_apellido', 
                        'terceros.segundo_apellido as segundo_apellido','terceros.primer_nombre as primer_nombre', 
                        'terceros.otros_nombres as otros_nombres',
                        'terceros.correo_electronico as correo_electronico', 
                        'tipos_de_documento_identidad.descripcion as tipos_de_documento_identidad',
                        'tipos_de_documento_identidad.id as tipo_de_documento_identidad_id',
                        'countries.id as id_country','countries.nombre as pais_empleo',
                        'countries.abreviacion as abreviacion',
                        'areas.id as id_area','areas.descripcion as area_descripcion',
                        'empleados.id as id_empleado','empleados.estado as estado',
                        'empleados.fecha_ingreso as fecha_ingreso',
                        'empleados.created_at as created_at',
                        'empleados.updated_at as updated_at')
                ->join('terceros', 'empleados.id_tercero', '=', 'terceros.id')
                ->join('tipos_de_documento_identidad', 'terceros.tipo_de_documento_identidad_id', '=', 'tipos_de_documento_identidad.id')
                ->join('countries', 'empleados.id_pais_del_empleo', '=', 'countries.id')
                ->join('areas', 'empleados.id_area', '=', 'areas.id')

                /**
                 * Validaciones para hacer consultas y filtrar dependiendo de la 
                 * opcion seleccionada en filtrar por, puede ser documento de identidad
                 * primer nombre, primer apellido etc y retornar respuesta con paginacion y
                 * los resultados de la busqueda, por defecto la paginacion esta en 10.
                 * Si no hay parametros de busqueda se hace la respuesta con todos los registros
                 */
                ->when($validacion_de_parametros_de_busqueda, function($query) use ($request){
                    return $query->where("terceros.$request->filtrar_por", "LIKE", "%$request->busqueda%");
                })->orderBy('empleados.id', 'DESC')
                ->paginate(10)
                ->appends(request()->query());
        } catch (Exception $e) {
            Log::error('Query empleados error: '.$e->getMessage());
            return response()->json(['error' => $e->getMessage()], 500);
        }
    
        return $empleados;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeesStoreRequest $request)
    {

        $validated_data = $request->validated();
        // Store
        # source: https://stackoverflow.com/a/37075806
        
        Terceros::create($validated_data)
        ->empleado()->create($validated_data);

        return response()->json([
            'success' => true,
            'message' => 'Empleado guardado exitosamente'
        ],201);   
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeesStoreRequest $request)
    {
        $validated_data = $request->validated();

        // Update empleados table data
        # source: https://stackoverflow.com/a/47389027
        Terceros::find($request->id)
            ->empleado()->update([
                'fecha_ingreso'=>       $validated_data['fecha_ingreso'],
                'id_pais_del_empleo'=>  $validated_data['id_pais_del_empleo'],
                'id_area'=>             $validated_data['id_area']
        ]);

        // update terceros table data
        Terceros::where('id',$request->id)
            ->update([
                'documento_identidad'=>             $validated_data['documento_identidad'],
                'primer_apellido'=>                 $validated_data['primer_apellido'],
                'segundo_apellido'=>                $validated_data['segundo_apellido'],
                'primer_nombre'=>                   $validated_data['primer_nombre'],
                'otros_nombres'=>                   $validated_data['otros_nombres'],
                'correo_electronico'=>              $validated_data['correo_electronico'],
                'tipo_de_documento_identidad_id'=>  $validated_data['tipo_de_documento_identidad_id']
        ]);

        return response()->json([
            'success' => true,
            'message' => 'Empleado modificado exitosamente'
        ]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        /**
         * Eliminar registro en tablas terceros y empleados.
         */

        $tercero = Terceros::findOrFail($request->id);
        $eliminar_tercero = Terceros::destroy($tercero->id);

        return response()->json([
            'success' => true,
            'message' => 'Empleado eliminado exitosamente'
        ]);   
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function validateSameEmail(Request $request)
    {
        /**
         * Condicion para verificar si se va amodificar un
         * registro y si ya tiene un correo electronico 
         * permitir registrar el mismo sino generar uno nuevo
         */
        if($request->id_tercero != ""){
            
            $condicion_consulta_tercero = [
                ['id', '=', $request->id_tercero],
                ['primer_nombre', '=', $request->primer_nombre],
                ['primer_apellido', '=', $request->primer_apellido],
            ];

            $cantidad_correos = $this->consultas_empleado->consultarSiYaExisteCorreoElectronico($condicion_consulta_tercero);
            
            if($cantidad_correos){
                return [
                    "ok" => true, 
                    "success" => true, 
                    "correo_electronico" => $cantidad_correos->correo_electronico
                ];
            }
        }
        
        $condicion_consulta_tercero = [
            ['primer_nombre', '=', $request->primer_nombre],
            ['primer_apellido', '=', $request->primer_apellido],
        ];

        $cantidad_correos = $this->consultas_empleado->consultarSiYaExisteCorreoElectronico($condicion_consulta_tercero);

        $correo_electronico = $this->correo_electronico_tercero->generarCorreoElectronicoSecuencial($cantidad_correos, $request);

        return [
            "ok" => true, 
            "success" => true, 
            "correo_electronico" => $correo_electronico
        ];
    
    }
}
