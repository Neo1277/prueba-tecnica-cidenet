<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Models\Terceros;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;

class EmployeesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       $dates = getDatesForValidation();

        $rules =  [
            'documento_identidad' => [
                'required',
                'alpha_num',
                'max:20',
                Rule::unique('terceros')->where(function ($query) {
                   return $query->where(
                        [
                            ['documento_identidad', '=', $this->documento_identidad],
                            ['tipo_de_documento_identidad_id', '=', $this->tipo_de_documento_identidad_id],
                        ]
                    );
            })],
            'primer_apellido' => 'required|max:20|regex:/^[A-Z ]+$/u',
            'segundo_apellido' => 'required|max:20|regex:/^[A-Z ]+$/u',
            'primer_nombre' => 'required|alpha|max:20|regex:/^[A-Z]+$/u',
            'otros_nombres' => 'max:50|nullable|regex:/^[A-Z ]+$/u',
            'correo_electronico' => 'required|email|unique:terceros,correo_electronico,'.$this->id.'|max:300',
            'fecha_ingreso' => 'required|before_or_equal:' . $dates['current_Date'].'|after_or_equal:'.$dates['previous_date'],
            'tipo_de_documento_identidad_id' => 'required',
            'id_pais_del_empleo' => 'required',
            'id_area' => 'required',
        ];

        if (in_array($this->method(), ['PUT', 'PATCH'])) {
            $product = $this->route()->parameter('product');

            $rules['documento_identidad'] = [
                'required',
                'alpha_num',
                'max:20',
                Rule::unique('terceros')->where(function ($query) {
                   return $query->where(
                        [
                            ['documento_identidad', '=', $this->documento_identidad],
                            ['tipo_de_documento_identidad_id', '=', $this->tipo_de_documento_identidad_id],
                        ]
                    );
                })->ignore($this->id)
            ];
        }

        return $rules;
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param  \Illuminate\Contracts\Validation\Validator  $validator
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     * source: https://emekambah.medium.com/taking-control-of-laravel-formrequest-response-abdea97d3475
     */
    protected function failedValidation(Validator $validator)
    {

        $response = response()->json([
            'success' => false,
            'message' => 'Ops! Some errors occurred',
            'errors' => $validator->errors()
        ],400);   
            
        
        throw (new ValidationException($validator, $response))
            ->errorBag($this->errorBag)
            ->redirectTo($this->getRedirectUrl());
    }    
}

function getDatesForValidation(){
    /**
    * Operaciones con fechas para validar si la fecha
    * de ingreso es hasta un mes menor e igual o menor
    * a la fecha actual
    */
    $fecha_actual = date("Y-m-d H:i:s");

    $fecha_anterior = strtotime('-1 months', strtotime($fecha_actual));
    $fecha_anterior = date('Y-m-d H:i:s' , $fecha_anterior);

    $dates = array(
        'previous_date' => $fecha_anterior,
        'current_Date' => $fecha_actual
    );

    return $dates;
}