<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Terceros extends Model
{
    use HasFactory;
    protected $fillable = ['documento_identidad','tipo_de_documento_identidad_id',
                        'primer_apellido','segundo_apellido','primer_nombre',
                        'otros_nombres','correo_electronico'];
    /**
     * Get the empleado associated with the Tercero.
     */
    public function empleado()
    {
        return $this->hasOne(Empleados::class,'id_tercero');
    }
}
