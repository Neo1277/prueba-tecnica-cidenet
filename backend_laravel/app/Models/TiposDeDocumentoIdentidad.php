<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TiposDeDocumentoIdentidad extends Model
{
    use HasFactory;

    /**
     * Definir nombre de tabla en la base de datos
     * debido a que no conincidio cuando trata
     * de hacer el match para hacer la consulta en la tabla
     * https://es.stackoverflow.com/a/356737/130524
     */
    protected $table = "tipos_de_documento_identidad";
    
    protected $fillable = ['codigo','descripcion'];
}
