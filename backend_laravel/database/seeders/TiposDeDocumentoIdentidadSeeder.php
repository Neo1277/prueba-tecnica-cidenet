<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TiposDeDocumentoIdentidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('tipos_de_documento_identidad')->insert([
            [
                "codigo" => "CC",
                "descripcion" => "Cedula de ciudadania"
            ],
            [
                "codigo" => "CE",
                "descripcion" => "Cedula de extranjeria"
            ],
            [
                "codigo" => "NIP",
                "descripcion" => "Numero de identificacion personal"
            ],
            [
                "codigo" => "NIT",
                "descripcion" => "Numero de identificacion tributaria"
            ],
            [
                "codigo" => "TI",
                "descripcion" => "Tarjeta de identidad"
            ],
            [
                "codigo" => "PAP",
                "descripcion" => "Pasaporte"
            ]
        ]);        
    }
}
