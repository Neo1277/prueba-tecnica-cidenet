<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ClientesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $current_date = date('Y-m-d H:i:s');
        \DB::table('terceros')->insert([
            [
                "documento_identidad" => "1144582119",
                "primer_apellido" => "CARDONA",
                "segundo_apellido" => "ALVAREZ",
                "primer_nombre" => "KARINA",
                "otros_nombres" => "ALBA",
                "correo_electronico" => "karina.cardona@cidenet.com.co",
                "tipo_de_documento_identidad_id" => "1",
                'created_at' => $current_date
            ],
            [
                "documento_identidad" => "1115982115",
                "primer_apellido" => "TRUJILLO",
                "segundo_apellido" => "GONZALEZZ",
                "primer_nombre" => "TULIO",
                "otros_nombres" => "DE LOS ANGELES",
                "correo_electronico" => "tulio.trujillo@cidenet.com.co",
                "tipo_de_documento_identidad_id" => "1",
                'created_at' => $current_date
            ]
        ]);      

        \DB::table('empleados')->insert([
            [
                "id_tercero" => 1,
                "id_pais_del_empleo" => 47,
                "id_area" => 1,
                "fecha_ingreso" => "2022-07-04 00:00:00",
                'created_at' => $current_date
            ],
            [
                "id_tercero" => 2,
                "id_pais_del_empleo" => 47,
                "id_area" => 3,
                "fecha_ingreso" => "2022-07-04 00:00:00",
                'created_at' => $current_date
            ],
        ]);      
    }
}
