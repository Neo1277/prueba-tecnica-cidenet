<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        return \DB::table('areas')->insert([
            [
                "descripcion" => "Administracion"
            ],
            [
                "descripcion" => "Financiera"
            ],
            [
                "descripcion" => "Compras"
            ],
            [
                "descripcion" => "Infraestructura"
            ],
            [
                "descripcion" => "Operacion"
            ],
            [
                "descripcion" => "Talento Humano"
            ],
            [
                "descripcion" => "Servicios varios"
            ]
        ]);        
    }
}
