<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     * Seeder Link: https://www.upiicsafordummies.com/como-crear-un-seeder-con-todos-los-paises-del-mundo-en-laravel/
     * Command delete all and migrate and seed again php artisan migrate:fresh --seed
     * call seeder: php artisan db:seed
     * https://es.stackoverflow.com/a/306424/130524
     * Save registers from code automatically
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(TiposDeDocumentoIdentidadSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(AreasSeeder::class);
        $this->call(ClientesSeeder::class);
    }
}
