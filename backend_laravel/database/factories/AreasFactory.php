<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use Faker\Generator as Faker;
use App\Models\Areas;

class AreasFactory extends Factory
{    
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Areas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'descripcion' => $this->faker->text,
        ];
    }
}
