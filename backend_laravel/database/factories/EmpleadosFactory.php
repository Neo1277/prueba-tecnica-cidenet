<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Areas;
use App\Models\Countries;
use App\Models\Terceros;
use App\Models\Empleados;

class EmpleadosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Empleados::class;    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fecha_ingreso'=>                   $this->faker->fecha_ingreso,
            'id_tercero'=>                      Terceros::all()->random()->id,
            'id_pais_del_empleo'=>              Countries::all()->random()->id,
            'id_area'=>                         Areas::all()->random()->id
        ];
    }
}
