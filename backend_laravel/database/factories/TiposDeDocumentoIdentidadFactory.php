<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\TiposDeDocumentoIdentidad;

class TiposDeDocumentoIdentidadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TiposDeDocumentoIdentidad::class;        
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'codigo' => strtoupper($this->faker->unique()->lexify('??')),
            'descripcion' => $this->faker->text,
        ];
    }
}
