<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Terceros;
use App\Models\TiposDeDocumentoIdentidad;

class TercerosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Terceros::class;    
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'documento_identidad'=>             $this->faker->unique()->numerify('##########'),
            'primer_apellido'=>                 $this->faker->lastName,
            'segundo_apellido'=>                $this->faker->lastName,
            'primer_nombre'=>                   $this->faker->firstName,
            'otros_nombres'=>                   $this->faker->name,
            'correo_electronico'=>              $this->faker->unique()->safeEmail,
            'tipo_de_documento_identidad_id'=>  TiposDeDocumentoIdentidad::all()->random()->id
        ];
    }
}
