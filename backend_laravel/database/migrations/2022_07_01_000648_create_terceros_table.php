<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTercerosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('terceros', function (Blueprint $table) {
            $table->id();
            $table->char('documento_identidad', 20);
            $table->unsignedBigInteger('tipo_de_documento_identidad_id'); // Foreign Key Link: https://makitweb.com/how-to-add-foreign-key-in-migration-laravel-8/
            $table->string('primer_apellido', 20);
            $table->string('segundo_apellido', 20);
            $table->string('primer_nombre', 20);
            $table->string('otros_nombres', 50)->nullable();
            $table->string('correo_electronico', 300)->unique();
            $table->foreign('tipo_de_documento_identidad_id')
                    ->references('id')->on('tipos_de_documento_identidad')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('terceros');
    }
}
