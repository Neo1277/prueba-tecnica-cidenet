<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmpleadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empleados', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_tercero'); // Foreign Key Link: https://makitweb.com/how-to-add-foreign-key-in-migration-laravel-8/
            $table->unsignedBigInteger('id_pais_del_empleo'); 
            $table->unsignedBigInteger('id_area'); 
            $table->timestamp("fecha_ingreso");
            $table ->enum('estado',['0','1'])->default('1')->comment('0 -> Inactivo, 1 -> Activo');
            $table->foreign('id_tercero')
                    ->references('id')->on('terceros')->onDelete('cascade');
            $table->foreign('id_pais_del_empleo')
                    ->references('id')->on('countries')->onDelete('cascade');
            $table->foreign('id_area')
                    ->references('id')->on('areas')->onDelete('cascade');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empleados');
    }
}
