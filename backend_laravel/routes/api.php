<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmpleadosController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\AreasController;
use App\Http\Controllers\TiposDeDocumentoIdentidadController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Empleados routes
Route::get('/empleados', [EmpleadosController::class,'index']); //Show all registers
Route::post('/empleados', [EmpleadosController::class,'store']); // Create a register
Route::put('/empleados/{id}', [EmpleadosController::class,'update']); // Update a register
Route::delete('/empleados/{id}', [EmpleadosController::class,'destroy']); // Delete a register
Route::post('/empleados-validarcorreo', [EmpleadosController::class,'validateSameEmail']); // validar correo electronico y generar correo

Route::get('/countries', [CountriesController::class,'index']); //Show all registers

Route::get('/areas', [AreasController::class,'index']); //Show all registers

Route::get('/tipos_de_documento_identidad', [TiposDeDocumentoIdentidadController::class,'index']); //Show all registers