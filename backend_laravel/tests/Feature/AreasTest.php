<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Areas;

class AreasTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        
        $this->firstArea = Areas::factory()->create([
            'descripcion' => 'Administration'
        ]);

    }
    /**
     * Obtener todas las areas
     */
    public function testRetrieveAreasSuccessfully()
    {
        /**
         * Test Get request de forma parcial
         * La api retornara muchos datos anidados
         */
        $this->json('GET', 'api/areas', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "descripcion"=> "Administration"
        ]);
    }
}
