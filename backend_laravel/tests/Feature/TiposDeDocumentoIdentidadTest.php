<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\TiposDeDocumentoIdentidad;

class TiposDeDocumentoIdentidadTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->firstTipoDeDocumentoIdentidad = TiposDeDocumentoIdentidad::factory()->create([
            'codigo' => 'CC',
            'descripcion' => 'Cedula de ciudadania'
        ]);

    }
    /**
     * Obtener todos los tipos de documento identidad
     */
    public function testRetrieveTiposDeDocumentoIdentidadSuccessfully()
    {
        /**
         * Test Get request de forma parcial
         * La api retornara muchos datos anidados
         */
        $this->json('GET', 'api/tipos_de_documento_identidad', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "descripcion"=> "Cedula de ciudadania"
        ]);
    }
}
