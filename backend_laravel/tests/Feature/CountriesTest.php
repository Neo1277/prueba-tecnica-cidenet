<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Countries;

class CountriesTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        
        $this->firstCountry = Countries::factory()->create([
            'abreviacion' => 'co',
            'nombre' => 'Colombia'
        ]);

        $this->secondCountry = Countries::factory()->create([
            'abreviacion' => 'us',
            'nombre' => 'United States of America'
        ]);

    }
    /**
     * Obtener todos los paises
     */
    public function testRetrieveCountriesSuccessfully()
    {
        /**
         * Test Get request de forma parcial
         * La api retornara muchos datos anidados
         */
        $this->json('GET', 'api/countries', [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "nombre"=> "Colombia",
                "abreviacion"=> "co"
        ]);
    }
}
