<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Models\Areas;
use App\Models\TiposDeDocumentoIdentidad;
use App\Models\Countries;
use App\Models\Terceros;

/**
 * Ejecutar metodo windows
 * .\vendor\bin\phpunit --filter testRetrieveClientesSuccessfully tests\Feature\EmpleadosTest.php
 * Linux:
 * vendor/bin/phpunit --filter testRetrieveClientesSuccessfully tests\Feature\EmpleadosTest.php
 */
class EmpleadosTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        
        $this->firstArea = Areas::factory()->create([
            'descripcion' => 'Administration'
        ]);

        $this->firstCountry = Countries::factory()->create([
            'abreviacion' => 'co',
            'nombre' => 'Colombia'
        ]);

        $this->secondCountry = Countries::factory()->create([
            'abreviacion' => 'us',
            'nombre' => 'United States of America'
        ]);

        $this->firstTipoDeDocumentoIdentidad = TiposDeDocumentoIdentidad::factory()->create([
            'codigo' => 'CC',
            'descripcion' => 'Cedula de ciudadania'
        ]);

    }

    /**
     * Obtener todos los empleados con paginacion
     */
    public function testRetrieveEmpleadosSuccessfully()
    {
        /**
         * Test Get request de forma parcial
         * La api retornara muchos datos anidados
         */

        $terceroData =
        [
            'documento_identidad'=>             '11122233444',
            'primer_apellido'=>                 'VELASCO',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.velasco@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        
        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->firstEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $this->json('GET', 'api/empleados', [], ['Accept' => 'application/json'])
            ->assertStatus(200);
    }

    /**
     * Obtener empleados filtrados por busqueda con paginacion
     */
    public function testRetrieveEmpleadosConBusquedaSuccessfully()
    {
        /**
         * Test Get request de forma parcial
         * La api retornara muchos datos anidados
         */
        $terceroData =
        [
            'documento_identidad'=>             '1112223344555',
            'primer_apellido'=>                 'VELASCO',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.velasco.1@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        
        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->secondEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        
        $filtrar_por = "primer_nombre";
        $busqueda = "KARLA";
        $query = "?filtrar_por=$filtrar_por&busqueda=$busqueda";

        $this->json('GET', 'api/empleados'.$query, [], ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
    
    public function testEmpleadoCreatedSuccessfully()
    {
        
        $fecha_actual = date("Y-m-d H:i:s");
        $datos_empleado = [
            "documento_identidad"=> "1778512597",
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> "ROJAS",
            "segundo_apellido"=> "CASTILLO",
            "primer_nombre"=> "ALEJANDRO",
            "otros_nombres"=> "MIGUEL DE LOS ANGELES",
            "correo_electronico"=> "alejandro.rojas@cidenet.com.co",
            "id_pais_del_empleo"=> $this->firstCountry->id,
            "id_area"=>  $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('POST', 'api/empleados', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(201);
    }

    public function testEmpleadoUpdatedSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '11hj344555',
            'primer_apellido'=>                 'MONTOYA',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.montoya@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        
        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->thirdEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $datos_empleado = [
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            'documento_identidad'=>             '11hj344555',
            'primer_apellido'=>                 'MONTOYA',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'STEFANY',
            'correo_electronico'=>              'karla.montoya@cidenet.com.co',
            "id_pais_del_empleo"=>      $this->firstCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('PUT', 'api/empleados/' . $this->thirdEmpleado->id, $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(200);
    }
    
    public function testEmpleadoDeletedSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '118530555',
            'primer_apellido'=>                 'VALENCIA',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.valencia@cidenet.com.us',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->secondCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->fourthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $this->json('DELETE', 'api/empleados/' . $this->fourthEmpleado->id, [], ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "success"=> true
            ]);
    }

    /**
     * Errores formato invalido en nombres y apellidos
     * y tambien correo electronico que ya existe
     */
    public function testEmpleadoCreatedUnSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '11865422555',
            'primer_apellido'=>                 'SANCHEZ',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.sanchez@cidenet.com.us',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->secondCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->fifthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $this->fifthEmpleado = Terceros::find($this->fifthEmpleado->id);

        $fecha_actual = date("Y-m-d H:i:s");
        $datos_empleado = [
            "documento_identidad"=> "1778512597",
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> "SANCHEZ",
            "segundo_apellido"=> "CASTILLO",
            "primer_nombre"=> "karla",
            "otros_nombres"=> "NIÑA",
            "correo_electronico"=> $this->fifthEmpleado->correo_electronico,
            "id_pais_del_empleo"=> $this->secondCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('POST', 'api/empleados', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }    

    /**
     * Error al registrar un documento de identidad y tipo
     * de documento de identidad que ya existen
     */
    public function testEmpleadoSameDocumentoIdentidadAndTipoUnSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '177851294211',
            'primer_apellido'=>                 'LOPEZ',
            'segundo_apellido'=>                'MARTINEZ',
            'primer_nombre'=>                   'KARLA',
            'otros_nombres'=>                   'MARIA',
            'correo_electronico'=>              'karla.lopez@cidenet.com.us',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->secondCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->sixthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->sixthEmpleado = Terceros::find($this->sixthEmpleado->id);

        $datos_empleado = [
            "documento_identidad"=> $this->sixthEmpleado->documento_identidad,
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> "ALOS",
            "segundo_apellido"=> "CASTILLO",
            "primer_nombre"=> "ALEJANDRO",
            "otros_nombres"=> "NINO",
            "correo_electronico"=> "alejandro.alos@cidenet.com.us",
            "id_pais_del_empleo"=> $this->secondCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('POST', 'api/empleados', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }        

    /**
     * Validacion si la fecha esta fuera del rango permitido
     * puede ser hasta un mes menor y menor o igual a la feha actual
     */
    public function testEmpleadoFechaFueraDeRangoPermitidoUnSuccessfully()
    {    
        $fechaActual = date("Y-m-d H:i:s");

        $fechaInvalida = strtotime('+1 day', strtotime($fechaActual));
        $fechaInvalida = date('Y-m-d H:i:s' , $fechaInvalida);

        $datos_empleado = [
            "documento_identidad"=> "177657878211",
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> "ALOS",
            "segundo_apellido"=> "CASTILLO",
            "primer_nombre"=> "ANGELA",
            "otros_nombres"=> "DANIELA",
            "correo_electronico"=> "angela.alos@cidenet.com.us",
            "id_pais_del_empleo"=> $this->secondCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fechaInvalida
        ];

        $this->json('POST', 'api/empleados', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }            

    /**
     * Errores formato invalido en nombres y apellidos
     * y tambien correo electronico que ya existe
     */
    public function testEmpleadoUpdatedUnSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '112209000642',
            'primer_apellido'=>                 'CASTANO',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'NICOL',
            'otros_nombres'=>                   'ALEJANDRA',
            'correo_electronico'=>              'nicol.castano@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->seventhEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $this->seventhEmpleado = Terceros::find($this->seventhEmpleado->id);

        $terceroData =
        [
            'documento_identidad'=>             '1177889000642',
            'primer_apellido'=>                 'HOYOS',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'NICOL',
            'otros_nombres'=>                   'ALEJANDRA',
            'correo_electronico'=>              'nicol.hoyos@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->eighthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->eighthEmpleado = Terceros::find($this->eighthEmpleado->id);

        $datos_empleado = [
            "documento_identidad"=> $this->eighthEmpleado->documento_identidad,
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> $this->eighthEmpleado->primer_apellido,
            "segundo_apellido"=> $this->eighthEmpleado->segundo_apellido,
            "primer_nombre"=> "hugo",
            "otros_nombres"=> "NIÑO",
            "correo_electronico"=> $this->seventhEmpleado->correo_electronico,
            "id_pais_del_empleo"=> $this->firstCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('PUT', 'api/empleados/' . $this->eighthEmpleado->id, $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }    

    /**
     * Error al modificar e ingresar un de documento de identidad y tipo
     * de documento de identidad que ya existen
     */
    public function testEmpleadoUpdateSameDocumentoIdentidadAndTipoUnSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '4465651121',
            'primer_apellido'=>                 'HOYOS',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'DAYANA',
            'otros_nombres'=>                   'ALEJANDRA',
            'correo_electronico'=>              'dayana.hoyos@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->ninethEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->ninethEmpleado = Terceros::find($this->ninethEmpleado->id);

        $terceroData =
        [
            'documento_identidad'=>             '9865651121',
            'primer_apellido'=>                 'HOYOS',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'NATALIA',
            'otros_nombres'=>                   'ALEJANDRA',
            'correo_electronico'=>              'natalia.hoyos@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->tenthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->tenthEmpleado = Terceros::find($this->tenthEmpleado->id);

        $datos_empleado = [
            "documento_identidad"=> $this->ninethEmpleado->documento_identidad,
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> $this->tenthEmpleado->primer_apellido,
            "segundo_apellido"=> $this->tenthEmpleado->segundo_apellido,
            "primer_nombre"=>  $this->tenthEmpleado->primer_nombre,
            "otros_nombres"=>  $this->tenthEmpleado->otros_nombres,
            "correo_electronico"=> $this->tenthEmpleado->correo_electronico,
            "id_pais_del_empleo"=> $this->firstCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fecha_actual
        ];

        $this->json('PUT', 'api/empleados/' . $this->tenthEmpleado->id, $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }        

    /**
     * Validacion si la fecha esta fuera del rango permitido
     * puede ser hasta un mes menor y menor o igual a la feha actual
     */
    public function testEmpleadoUpdateFechaFueraDeRangoPermitidoUnSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '9822251121',
            'primer_apellido'=>                 'HOYOS',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'CARLOS',
            'otros_nombres'=>                   '',
            'correo_electronico'=>              'carlos.hoyos@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fechaActual = date("Y-m-d H:i:s");

        $fechaInvalida = strtotime('-2 months', strtotime($fechaActual));
        $fechaInvalida = date('Y-m-d H:i:s' , $fechaInvalida);

        $empleadoData =
        [
            'fecha_ingreso'=>  $fechaActual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->eleventhEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->eleventhEmpleado = Terceros::find($this->eleventhEmpleado->id);

        $datos_empleado = [
            "documento_identidad"=> $this->eleventhEmpleado->documento_identidad,
            "tipo_de_documento_identidad_id"=> $this->firstTipoDeDocumentoIdentidad->id,
            "primer_apellido"=> $this->eleventhEmpleado->primer_apellido,
            "segundo_apellido"=> $this->eleventhEmpleado->segundo_apellido,
            "primer_nombre"=>  $this->eleventhEmpleado->primer_nombre,
            "otros_nombres"=>  $this->eleventhEmpleado->otros_nombres,
            "correo_electronico"=> $this->eleventhEmpleado->correo_electronico,
            "id_pais_del_empleo"=> $this->firstCountry->id,
            "id_area"=> $this->firstArea->id,
            "fecha_ingreso"=> $fechaInvalida
        ];

        $this->json('PUT', 'api/empleados/' . $this->eleventhEmpleado->id, $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(400);
    }            



    
    /**
     * Generar correo electronico automaticamente teniendo en adicionar 
     * en cuenta nombres y apellidos y abreviacion de pais
     * para dominio
     */
    public function testEmpleadoValidarYGenerarEmailNuevoSuccessfully()
    {
        $datos_empleado = [
            "primer_apellido"=> "CARDONA",
            "primer_nombre"=> "KARINA",
            "abreviacion"=> "co"
        ];

        $this->json('POST', 'api/empleados-validarcorreo', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "success"=> true,
                "correo_electronico"=> "karina.cardona@cidenet.com.co"
            ]);
    }

    /**
     * Generar correo electronico automaticamente teniendo en adicionar 
     * en cuenta nombres y apellidos y abreviacion de pais
     * para dominio
     */
    public function testEmpleadoValidarYGenerarEmailYaExisteSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '1010434871',
            'primer_apellido'=>                 'GOMEZ',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'JOHANA',
            'otros_nombres'=>                   'ALEJANDRA',
            'correo_electronico'=>              'johana.gomez@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->twelfthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);

        $this->twelfthEmpleado = Terceros::find($this->twelfthEmpleado->id);

        $datos_empleado = [
            "primer_apellido"=> $this->twelfthEmpleado->primer_apellido,
            "primer_nombre"=> $this->twelfthEmpleado->primer_nombre,
            "abreviacion"=> "co"
        ];

        $this->json('POST', 'api/empleados-validarcorreo', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "success"=> true,
                "correo_electronico"=> "johana.gomez.1@cidenet.com.co"
            ]);
    }    
    /**
     * Generar correo electronico automaticamente teniendo en editar 
     * en cuenta nombres y apellidos y abreviacion de pais
     * para dominio
     */
    public function testEmpleadoValidarYGenerarEmailEnUpdateMismoNombreYApellidoSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '1415434821',
            'primer_apellido'=>                 'GOMEZ',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'JUAN',
            'otros_nombres'=>                   'ALEJANDRO',
            'correo_electronico'=>              'juan.gomez@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->thirteenthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->thirteenthEmpleado = Terceros::find($this->thirteenthEmpleado->id);

        $datos_empleado = [
            "primer_apellido"=> $this->thirteenthEmpleado->primer_apellido,
            "primer_nombre"=> $this->thirteenthEmpleado->primer_nombre,
            "abreviacion"=> "co",
            "id_tercero"=> $this->thirteenthEmpleado->id
        ];

        $this->json('POST', 'api/empleados-validarcorreo', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "success"=> true,
                "correo_electronico"=> "juan.gomez@cidenet.com.co"
            ]);
    }
    
    public function testEmpleadoValidarYGenerarEmailEnUpdateDiferenteNombreYApellidoSuccessfully()
    {
        $terceroData =
        [
            'documento_identidad'=>             '16178434821',
            'primer_apellido'=>                 'JIMENEZ',
            'segundo_apellido'=>                'LOPEZ',
            'primer_nombre'=>                   'JUAN',
            'otros_nombres'=>                   'MANUEL',
            'correo_electronico'=>              'juan.jimenez@cidenet.com.co',
            'tipo_de_documento_identidad_id'=>  $this->firstTipoDeDocumentoIdentidad->id
        ];

        $fecha_actual = date("Y-m-d H:i:s");

        $empleadoData =
        [
            'fecha_ingreso'=>  $fecha_actual,
            'id_pais_del_empleo'=>  $this->firstCountry->id,
            'id_area'=>  $this->firstArea->id
        ];
        
        $this->fourteenthEmpleado = Terceros::factory()->create($terceroData)
        ->empleado()->create($empleadoData);
        
        $this->fourteenthEmpleado = Terceros::find($this->fourteenthEmpleado->id);

        $datos_empleado = [
            "primer_apellido"=>  'PARRA',
            "primer_nombre"=> 'PABLO',
            "abreviacion"=> "co",
            "id_tercero"=> $this->fourteenthEmpleado->id
        ];

        $this->json('POST', 'api/empleados-validarcorreo', $datos_empleado, ['Accept' => 'application/json'])
            ->assertStatus(200)
            ->assertJsonFragment([
                "success"=> true,
                "correo_electronico"=> "pablo.parra@cidenet.com.co"
            ]);
    }

}
