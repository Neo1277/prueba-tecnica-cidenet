export const baseUrlApiRest = 'http://127.0.0.1:8000/api';

export const empleados = '/empleados';
export const countries = '/countries';
export const areas = '/areas';
export const tipos_de_documento_identidad = '/tipos_de_documento_identidad';
export const validar_email = '/empleados-validarcorreo';
