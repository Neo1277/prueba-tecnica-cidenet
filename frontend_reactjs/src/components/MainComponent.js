import React, { Component } from 'react';

import { 
  EmpleadosView
} from './EmpleadosComponents/EmpleadosView';
import { 
  AddEmpleadoComponent
} from './EmpleadosComponents/AdicionarEmpleadosView';
import { 
  EditEmpleadoComponent
} from './EmpleadosComponents/EditarEmpleadosView';
import { 
  DeleteEmpleadoComponent
} from './EmpleadosComponents/EliminarEmpleadosView';

import Header from './HeaderComponent';
import Footer from './FooterComponent';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { 
  fetchEmpleados, 
  registerEmpleado,
  editEmpleado,
  deleteEmpleado,
  fetchCountries,
  fetchAreas,
  fetcTiposDeDocumentoIdentidad,
  validarEmailEmpleado
} from '../redux/ActionCreators';

/* Set data gotten from Django API with redux to the Cpmponent's props */
const mapStateToProps = state => {
  return{
    empleados: state.empleados,
    countries: state.countries,
    areas: state.areas,
    tipos_de_documento_identidad: state.tipos_de_documento_identidad
  }
}

/* Set functions from ActionCreators redux to the Cpmponent's props and dispatch */
const mapDispatchToProps = (dispatch) => ({

  fetchEmpleados: (link, filtrar_por, busqueda) => { dispatch(fetchEmpleados(link, filtrar_por, busqueda))},
  registerEmpleado: (datosEmpleado) => dispatch(registerEmpleado(datosEmpleado)),
  editEmpleado: (datosEmpleado) => dispatch(editEmpleado(datosEmpleado)),
  deleteEmpleado: (empleadoId) => dispatch(deleteEmpleado(empleadoId)),

  validarEmailEmpleado: (datosEmpleado) => dispatch(validarEmailEmpleado(datosEmpleado)),

  fetchCountries: () => { dispatch(fetchCountries())},

  fetchAreas: () => { dispatch(fetchAreas())},

  fetcTiposDeDocumentoIdentidad: () => { dispatch(fetcTiposDeDocumentoIdentidad())},

});


class Main extends Component {

  //Execute this before render
  componentDidMount() {
    this.props.fetchEmpleados();
    this.props.fetchCountries();
    this.props.fetchAreas();
    this.props.fetcTiposDeDocumentoIdentidad();
  }

  render(){
    
    
    const EmpleadoWithId = ({match}) => {
      return(
        <EditEmpleadoComponent empleado={this.props.empleados.empleados.data.filter((empleado) => empleado.id_empleado == match.params.id)[0]} 
                              editEmpleado={this.props.editEmpleado} 
                              countries={this.props.countries} areas={this.props.areas} 
                              tipos_de_documento_identidad={this.props.tipos_de_documento_identidad}
                              validarEmailEmpleado={this.props.validarEmailEmpleado} 
        />
      );
    };

    const DeleteEmpleadoWithId = ({match}) => {
      return(
        <DeleteEmpleadoComponent empleado={this.props.empleados.empleados.data.filter((empleado) => empleado.id_empleado == match.params.id)[0]} 
                              deleteEmpleado={this.props.deleteEmpleado} 
                              countries={this.props.countries} areas={this.props.areas} 
                              tipos_de_documento_identidad={this.props.tipos_de_documento_identidad}
        />
      );
    };

    /**
     * Set routes to open the different pages calling the components
     * And redirect to home if the url that the user type in the browser
     * does not match with any url from here
     */

    return (
      <div>
        <Header />
          <Switch>
            <Route path='/home' component={() => <EmpleadosView empleados={this.props.empleados} fetchEmpleados={this.props.fetchEmpleados} />} />
            <Route path='/adicionar_empleado' component={() => <AddEmpleadoComponent registerEmpleado={this.props.registerEmpleado} 
                                                                countries={this.props.countries} areas={this.props.areas} 
                                                                tipos_de_documento_identidad={this.props.tipos_de_documento_identidad}
                                                                validarEmailEmpleado={this.props.validarEmailEmpleado} /> } />
            
            <Route path="/editar_empleado/:id" component={EmpleadoWithId} />
            <Route path="/eliminar_empleado/:id" component={DeleteEmpleadoWithId} />
            <Redirect to="/home" />
          </Switch>
        <Footer />
      </div>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));

