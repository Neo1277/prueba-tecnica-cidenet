import React, { Component }  from 'react';
import { Link } from "react-router-dom";
import { Loading } from '../LoadingComponent';
import { 
	Breadcrumb, 
	BreadcrumbItem,
	Button,
	Form, 
	FormGroup, 
	Input, 
    Label, 
} from 'reactstrap';
import { AddEmpleadoComponent } from './AdicionarEmpleadosView';
/**
 * Edit empleado component
 */
export class EditEmpleadoComponent extends AddEmpleadoComponent {

    constructor(props){
        super(props);

        this.handleUpdate = this.handleUpdate.bind(this);
        this.handlePrimerNombre = this.handlePrimerNombre.bind(this);
        this.handlePrimerApellido = this.handlePrimerApellido.bind(this);
        this.handleAbreviacion = this.handleAbreviacion.bind(this);
        this.validarEmailYCambiarState = this.validarEmailYCambiarState.bind(this);
        //this.handleValidacionEmail = this.handleValidacionEmail.bind(this);

	    this.state = {
            primer_nombre: this.props.empleado.primer_nombre,
            primer_apellido: this.props.empleado.primer_apellido,
            correo_electronico: this.props.empleado.correo_electronico,
            abreviacion: this.props.empleado.abreviacion
	    };

    }

    /**
     * 
     * Send parameters to do the register empleado request and clear fields in the view
     */

    handleUpdate(event) {
        event.preventDefault();

        this.handleValidations();
        
        // Metodo pasado por medio de props para acer llamado a la API y editar
        this.props.editEmpleado({
            tipo_de_documento_identidad_id: this.tipo_de_documento_identidad_id.value, 
            documento_identidad: this.documento_identidad.value, 
            primer_apellido: this.primer_apellido.value, 
            segundo_apellido: this.segundo_apellido.value, 
            primer_nombre: this.primer_nombre.value, 
            otros_nombres: this.otros_nombres.value,
            correo_electronico: this.correo_electronico.value,
            id_pais_del_empleo: this.id_pais_del_empleo.value,
            id_area: this.id_area.value,
            fecha_ingreso: this.fecha_ingreso.value,
            id_tercero: this.props.empleado.id_tercero,
            id_empleado: this.props.empleado.id_empleado
        });
        

    }    
    
    /**
     * LLamar metodo validarEmailEmpleado que hace request a la API
     * para generar correo automaticamente con los valores del state
     * y continuar el flujo del promise para obtener el correo, cambiar el state
     * y mostrarlo en el campo de texto de html
     * @param {*} datosState 
     */
    validarEmailYCambiarState(datosState) {
        
        /**
         * Cambiar state con uso de callback asincrono para poder ver el u
         * nuevo state
         * https://stackoverflow.com/a/34237449/9655579
         */
        this.setState(
            datosState
        , () => {
            this.props.validarEmailEmpleado({
                primer_apellido: this.state.primer_apellido, 
                primer_nombre: this.state.primer_nombre, 
                abreviacion: this.state.abreviacion, 
                id_tercero: this.props.empleado.id_tercero
            })
            .then(response => {
                if (response.ok) {
                  return response;
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
              },
              error => {
                    throw error;
              })
            .then(response => response.json())
            .then(response => { 
                console.log('Correo electronico obtenido', response); 
                //alert('Correo electronico obtenido!\n'+JSON.stringify(response.correo_electronico)); 
                this.setState({
                    correo_electronico: response.correo_electronico
                });
            })
            .catch(error =>  { console.log('Error al obtener correo', error.message); });

        });
    }

    /**
     * Render form with their respective validations
     */
    render(){
        
        if (this.props.countries.isLoading) {
		
            return(
                <Loading />
            );
        }
        else if (this.props.countries.errMess) {
            return(
                <h4>{this.props.errMess}</h4>
            );
        }
        else { 
            return(
                <div className="container">
                    
                <br />
				<div className="row">
					<div className="col">
						<Breadcrumb>
							<BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
							<BreadcrumbItem active>Editar empleado</BreadcrumbItem>
						</Breadcrumb>
					</div>
				</div>		
                <br />
                    <div className="row row-content">
                        <div className="col-12">
                            <h4>Editar Empleado</h4>
                        </div>
                        <div className="col-12 col-md-9">

                            <Form onSubmit={this.handleUpdate}>
                                <FormGroup>
                                    <Label for="tipo_de_documento_identidad_id">Tipo de documento identidad</Label>
                                    <Input type="select" name="tipo_de_documento_identidad_id" 
                                    id="tipo_de_documento_identidad_id" innerRef={(input) => this.tipo_de_documento_identidad_id = input}
                                    defaultValue={this.props.empleado.tipo_de_documento_identidad_id}>
                                    {this.props.tipos_de_documento_identidad.tipos_de_documento_identidad.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="documento_identidad">Documento de identidad</Label>
                                    <Input type="text" id="documento_identidad" name="documento_identidad"
                                        innerRef={(input) => this.documento_identidad = input}
                                        defaultValue={this.props.empleado.documento_identidad} 
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_apellido">Primer apellido</Label>
                                    <Input type="text" id="primer_apellido" name="primer_apellido"
                                        innerRef={(input) => this.primer_apellido = input} 
                                        defaultValue={this.props.empleado.primer_apellido}
                                        onChange={this.handlePrimerApellido} 
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="segundo_apellido">Segundo apellido</Label>
                                    <Input type="text" id="segundo_apellido" name="segundo_apellido"
                                        innerRef={(input) => this.segundo_apellido = input} 
                                        defaultValue={this.props.empleado.segundo_apellido}
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_nombre">Primer nombre</Label>
                                    <Input type="text" id="primer_nombre" name="primer_nombre"
                                        innerRef={(input) => this.primer_nombre = input} 
                                        defaultValue={this.props.empleado.primer_nombre}
                                        onChange={this.handlePrimerNombre} 
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="otros_nombres">Otros nombres</Label>
                                    <Input type="text" id="otros_nombres" name="otros_nombres"
                                        innerRef={(input) => this.otros_nombres = input} 
                                        defaultValue={this.props.empleado.otros_nombres} 
                                        maxLength={50} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="correo_electronico">Correo electronico</Label>
                                    <Input type="email" id="correo_electronico" name="correo_electronico"
                                        innerRef={(input) => this.correo_electronico = input}
                                        value={this.state.correo_electronico} 
                                        maxLength={300} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_pais_del_empleo">Pais del empleo</Label>
                                    <Input type="select" name="id_pais_del_empleo" 
                                    id="id_pais_del_empleo" innerRef={(input) => this.id_pais_del_empleo = input} 
                                    defaultValue={this.props.empleado.id_country}
                                    onClick={this.handleAbreviacion}>
                                    {this.props.countries.countries.map((field, i) => { 
                                        /*{
                                            if (i==0){
                                                this.setState({
                                                    abreviacion: field.abreviacion
                                                });
                                            }
                                        }*/
                                        return(
                                            <>
                                                <option value={field.id}>{field.nombre} - {field.abreviacion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_area">Area</Label>
                                    <Input type="select" name="id_area" 
                                    id="id_area" innerRef={(input) => this.id_area = input}
                                    defaultValue={this.props.empleado.id_area}>
                                    {this.props.areas.areas.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="fecha_ingreso">Fecha de ingreso</Label>
                                    <Input type="datetime-local" id="fecha_ingreso" name="fecha_ingreso"
                                        innerRef={(input) => this.fecha_ingreso = input}  
                                        defaultValue={this.props.empleado.fecha_ingreso} />
                                </FormGroup>
                                
                                <Button type="submit" value="submit" color="success">
                                    <span className="fa fa-pencil">&nbsp;</span>  
                                    Editar
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>
            );
        }
    }

}

