import React, { Component }  from 'react';
import { Link } from "react-router-dom";
import { Loading } from '../LoadingComponent';
import { 
	Breadcrumb, 
	BreadcrumbItem,
	Button,
	Form, 
	FormGroup, 
	Input, 
    Label, 
} from 'reactstrap';

/**
 * Delete empleado component
 */
 export class DeleteEmpleadoComponent extends Component {

    constructor(props){
        super(props);

        this.handleDelete = this.handleDelete.bind(this);

    }

    /**
     * 
     * Send parameters to do the register empleado request and clear fields in the view
     */

    handleDelete(event) {
        event.preventDefault();
        /**
         * Mostrar ventana de confirmacion antes de eliminar registro de 
         * la base de datos
         */
        var mensaje = 'Esta seguro que desea eliminar el empleado? Si / No';
        if (window.confirm(mensaje)) {
            this.props.deleteEmpleado(
                this.props.empleado.id_tercero
            );
        } else {
            
            console.log('NO eliminar');
        }
        

    }    

    /**
     * Render form with their respective validations
     */
    render(){
        
        if (this.props.countries.isLoading) {
		
            return(
                <Loading />
            );
        }
        else if (this.props.countries.errMess) {
            return(
                <h4>{this.props.errMess}</h4>
            );
        }
        else { 
            return(
                <div className="container">
                    
                <br />
				<div className="row">
					<div className="col">
						<Breadcrumb>
							<BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
							<BreadcrumbItem active>Eliminar empleado</BreadcrumbItem>
						</Breadcrumb>
					</div>
				</div>		
                <br />
                    <div className="row row-content">
                        <div className="col-12">
                            <h4>Eliminar Empleado</h4>
                        </div>
                        <div className="col-12 col-md-9">

                            <Form onSubmit={this.handleDelete}>
                                <FormGroup>
                                    <Label for="tipo_de_documento_identidad_id">Tipo de documento identidad</Label>
                                    <Input type="select" name="tipo_de_documento_identidad_id" 
                                    id="tipo_de_documento_identidad_id" innerRef={(input) => this.tipo_de_documento_identidad_id = input}
                                    value={this.props.empleado.tipo_de_documento_identidad_id}>
                                    {this.props.tipos_de_documento_identidad.tipos_de_documento_identidad.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="documento_identidad">Documento de identidad</Label>
                                    <Input type="text" id="documento_identidad" name="documento_identidad"
                                        innerRef={(input) => this.documento_identidad = input}
                                        value={this.props.empleado.documento_identidad} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_apellido">Primer apellido</Label>
                                    <Input type="text" id="primer_apellido" name="primer_apellido"
                                        innerRef={(input) => this.primer_apellido = input} 
                                        value={this.props.empleado.primer_apellido} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="segundo_apellido">Segundo apellido</Label>
                                    <Input type="text" id="segundo_apellido" name="segundo_apellido"
                                        innerRef={(input) => this.segundo_apellido = input} 
                                        value={this.props.empleado.segundo_apellido} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_nombre">Primer nombre</Label>
                                    <Input type="text" id="primer_nombre" name="primer_nombre"
                                        innerRef={(input) => this.primer_nombre = input} 
                                        value={this.props.empleado.primer_nombre}  />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="otros_nombres">Otros nombres</Label>
                                    <Input type="text" id="otros_nombres" name="otros_nombres"
                                        innerRef={(input) => this.otros_nombres = input} 
                                        value={this.props.empleado.otros_nombres} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="correo_electronico">Correo electronico</Label>
                                    <Input type="email" id="correo_electronico" name="correo_electronico"
                                        innerRef={(input) => this.correo_electronico = input}
                                        value={this.props.empleado.correo_electronico}  />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_pais_del_empleo">Pais del empleo</Label>
                                    <Input type="select" name="id_pais_del_empleo" 
                                    id="id_pais_del_empleo" innerRef={(input) => this.id_pais_del_empleo = input} 
                                    value={this.props.empleado.id_country}>
                                    {this.props.countries.countries.map((field, i) => { 
                                        /*{
                                            if (i==0){
                                                this.setState({
                                                    abreviacion: field.abreviacion
                                                });
                                            }
                                        }*/
                                        return(
                                            <>
                                                <option value={field.id}>{field.nombre} - {field.abreviacion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_area">Area</Label>
                                    <Input type="select" name="id_area" 
                                    id="id_area" innerRef={(input) => this.id_area = input}
                                    value={this.props.empleado.id_area}>
                                    {this.props.areas.areas.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="fecha_ingreso">Fecha de ingreso</Label>
                                    <Input type="datetime-local" id="fecha_ingreso" name="fecha_ingreso"
                                        innerRef={(input) => this.fecha_ingreso = input}  
                                        value={this.props.empleado.fecha_ingreso} />
                                </FormGroup>
                                
                                <Button type="submit" value="submit" color="danger">
                                    <span className="fa fa-trash">&nbsp;</span>  
                                    Eliminar
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>
            );
        }
    }

}
