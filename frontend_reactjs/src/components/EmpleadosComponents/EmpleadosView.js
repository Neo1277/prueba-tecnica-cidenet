import React, { Component }  from 'react';
import { Link } from "react-router-dom";
import { Loading } from '../LoadingComponent';
import { 
	Breadcrumb, 
	BreadcrumbItem,
	Button,
	Table,
	FormGroup, 
	Input, 
} from 'reactstrap';

// https://stackoverflow.com/a/60745732/9655579
export const removeExtraSpace = (string) => string.trim().split(/ +/).join(' ');

export class PaginationComponent extends Component {

    constructor(props){
        super(props);

        this.handlePagination = this.handlePagination.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.handleSearchWord = this.handleSearchWord.bind(this);

	    this.state = {
          filtrar_por: 'documento_identidad',
          busqueda: null
	    };
    }

    /**
     * 
     * Send parameters to do the post request and clear fields in the view
     */

	 handlePagination(event) {
        event.preventDefault();
         //alert(event.target.href)
         
        this.props.fetchEmpleados(
            event.target.href,
            this.state.filtrar_por,
            this.state.busqueda
        );
        

    }    
    /**
     * 
     * Send parameters to do the post request and clear fields in the view
     */

	 handleSearch(event) {
        event.preventDefault();

        /**
         * Validaciones para campo de texto busqueda antes de hacer
         * el request a la API Restful
         * Algunas de las validaciones son:
         * Expresiones regulares para solo permitir
         * letras, caracteres alfanumericos sin tildes ni caracteres
         * especiales
         */
        if (!this.busqueda.value || 
            this.busqueda.value.trim().length === 0) {
            alert('field cannot be empty');
        }else{

            var busqueda = removeExtraSpace(this.busqueda.value);
            
            if (/[^a-zA-Z0-9 ]/.test(busqueda) &&
            this.filtrar_por.value != 'correo_electronico') {
                alert('Caracteres invalidos');
             }else{
                this.setState({
                    filtrar_por: this.filtrar_por.value,
                    busqueda: busqueda
                })
                
                this.props.fetchEmpleados(
                    event.target.href,
                    this.state.filtrar_por,
                    busqueda
                );
             }


        }
        
    }    

    /**
     * Evento para obtener el valor del campo
     * filtrar_por para cambiar el state y porder 
     * pasar los parametros al metodo para hacer la busqueda
     * @param {*} event 
     */
    handleFilter(event) {

        event.preventDefault();
        this.setState({
            filtrar_por: event.target.value
        })
    }

    /**
     * Evento para obtener el valor del campo
     * busqueda para cambiar el state y porder 
     * pasar los parametros al metodo para hacer la busqueda
     * @param {*} event 
     */
    handleSearchWord(event) {
        event.preventDefault();

        this.setState({
            busqueda: event.target.value
        })
    }

    /**
     * Render form with their respective validations
     */
     render(){
        
        return(
            
            <div className="container">
                <div className="row">
                    <div className="col-3">
                        
                        <FormGroup>
                            {/*<Label for="category">Filtrar por</Label>*/}
                            <Input type="select" name="filtrar_por" id="filtrar_por" onChange={this.handleFilter}
                                innerRef={(input) => this.filtrar_por = input}>
                                <option value="documento_identidad">Documento de identidad</option>
                                <option value="tipos_de_documento_identidad">Tipo de documento de identidad</option>
                                <option value="primer_apellido">Primer apellido</option>
                                <option value="segundo_apellido">Segundo apellido</option>
                                <option value="primer_nombre">Primer nombre</option>
                                <option value="otros_nombres">Otros nombres</option>
                                <option value="pais_del_empleo">Pais del empleo</option>
                                <option value="correo_electronico">Correo electronico</option>
                            </Input>
                        </FormGroup>     
                        </div>                     
                    <div className="col-3">
                        <FormGroup>
                            <Input onChange={this.handleSearchWord} type="text" id="busqueda" name="busqueda"
                                innerRef={(input) => this.busqueda = input} />
                        </FormGroup></div>  
                    <div className="col-2">
                        <Button onClick={this.handleSearch} type="submit" value="submit" color="primary">
                            <span className="fa fa-search">&nbsp;</span>
                            Buscar
                        </Button>
                    </div>
                </div>
            <nav aria-label="Page navigation example">
                <ul className="pagination">
                    {this.props.links.map((field, i) => { 
                        return(
                                <li className="page-item">
                                    <a className="page-link" 
                                    onClick={this.handlePagination} 
                                    href={field.url}>
                                        {field.label}
                                    </a>
                                </li>
                        );
                    }) }
                </ul>
            </nav>
            </div>
        );
     }
}

/**
 * Show status if the page is Loading shows spinner else shows error or the page content
 */
 export const EmpleadosView = (props) => {
	//console.log(props);
	
	if (props.empleados.isLoading) {
		
        return(
            <Loading />
        );
    }
    else if (props.empleados.errMess) {
        return(
            <h4>{props.errMess}</h4>
        );
    }
	else { 
		/**
		 * Iterate over object that is in the store
		 */
		return(
			<div className="container">
				<br />
				<div className="row">
					<div className="col">
						<Breadcrumb>
							<BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
							<BreadcrumbItem active>Empleados</BreadcrumbItem>
						</Breadcrumb>
					</div>
				</div>				
				<h2 align="center">Empleados</h2>

				<Link to='/adicionar_empleado' >
					<Button color="primary">
                        <span className="fa fa-plus-square">&nbsp;</span> 
                        Adicionar empleado
                    </Button>
                </Link>				

				<div className="row row-content">
					<Table responsive>
						<thead>
							<tr>
							<th>Tipo de documento de identidad</th>
							<th>Documento de identidad</th>
							<th>Primer apellido</th>
							<th>Segundo apellido</th>
							<th>Primer nombre</th>
							<th>Otros nombres</th>
							<th>Correo electronico</th>
							<th>Pais del empleo</th>
							<th>Fecha de ingreso</th>
							<th>Fecha y hora de registro</th>
							<th>Fecha y hora de edicion</th>
							<th>Estado</th>
							<th>Action</th>
							</tr>
						</thead>
						<tbody>
					{props.empleados.empleados.data.map((field, i) => { 
						
						return(
							<tr key={field.id_empleado}>
                                <td>{field.tipos_de_documento_identidad}</td>
                                <td>{field.documento_identidad}</td>
								<td>{field.primer_apellido}</td>
								<td>{field.segundo_apellido}</td>
								<td>{field.primer_nombre}</td>
								<td>{field.otros_nombres}</td>
								<td>{field.correo_electronico}</td>
								<td>{field.pais_empleo}</td>
								<td>{field.fecha_ingreso}</td>
								<td>{field.created_at}</td>
								<td>{field.updated_at}</td>
								<td>
                                {field.estado == '1' ?
                                        <>Activo</>
                                    :
                                        <>Inactivo</>
                                }
                                </td>
								<td>
									<Link to={`/editar_empleado/${field.id_empleado}`} >
										<Button color="success">
                                            <span className="fa fa-pencil"></span>
                                        </Button>
									</Link>
									<Link to={`/eliminar_empleado/${field.id_empleado}`} >
										<Button color="danger">
                                            <span className="fa fa-trash"></span>
                                        </Button>
                                    </Link>
                                </td>
							</tr>
						);
					}) }
						</tbody>
					</Table>

				</div>

                <PaginationComponent fetchEmpleados={props.fetchEmpleados} links={props.empleados.empleados.links}  />

			</div>
        );
	}
}
