import React, { Component }  from 'react';
import { Link } from "react-router-dom";
import { Loading } from '../LoadingComponent';
import { 
	Breadcrumb, 
	BreadcrumbItem,
	Button,
	Form, 
	FormGroup, 
	Input, 
    Label,
} from 'reactstrap';
import { removeExtraSpace } from './EmpleadosView';


export class AddEmpleadoComponent extends Component {

    constructor(props){
        super(props);

        this.handleRegister = this.handleRegister.bind(this);
        this.handlePrimerNombre = this.handlePrimerNombre.bind(this);
        this.handlePrimerApellido = this.handlePrimerApellido.bind(this);
        this.handleAbreviacion = this.handleAbreviacion.bind(this);
        this.validarEmailYCambiarState = this.validarEmailYCambiarState.bind(this);
        //this.handleValidacionEmail = this.handleValidacionEmail.bind(this);

	    this.state = {
            primer_nombre: null,
            primer_apellido: null,
            correo_electronico: null,
            abreviacion: 'af'
	    };

    }

    handleValidations(){

        /**
         * Validaciones para los campos de texto antes de hacer
         * el request a la API Restful
         * Algunas de las validaciones son:
         * Expresiones regulares para solo permitir mayusculas,
         * letras, caracteres alfanumericos sin tildes ni caracteres
         * especiales
         */

         if (!this.documento_identidad.value || 
            this.documento_identidad.value.trim().length === 0) {
            alert('documento identidad_validado apellido field cannot be empty');
        }else{
            this.documento_identidad.value = removeExtraSpace(this.documento_identidad.value);
            
            if (/[^a-zA-Z0-9 ]/.test(this.documento_identidad.value) ) {
                alert('Caracteres invalidos documento identidad: solo se permiten valores alfanumericos');
                return false;
             }
        }        
        
        if (!this.primer_apellido.value || 
            this.primer_apellido.value.trim().length === 0) {
            alert('primer apellido field cannot be empty');
        }else{
            this.primer_apellido.value = removeExtraSpace(this.primer_apellido.value);
            
            if (/[^A-Z ]/.test(this.primer_apellido.value) ) {
                alert('primer apellido Caracteres invalidos: solo se permiten letras mayusculas sin tildes y tampoco se permiten letras con caracteres especiales');
                return false;
             }
        }        
        
        if (!this.segundo_apellido.value || 
            this.segundo_apellido.value.trim().length === 0) {
            alert('segundo apellido field cannot be empty');
        }else{
            this.segundo_apellido.value = removeExtraSpace(this.segundo_apellido.value);
            
            if (/[^A-Z ]/.test(this.segundo_apellido.value) ) {
                alert('segundo apellido Caracteres invalidos: solo se permiten letras mayusculas sin tildes y tampoco se permiten letras con caracteres especiales');
                return false;
             }
        }        
        
        if (!this.primer_nombre.value || 
            this.primer_nombre.value.trim().length === 0) {
            alert('Primer nombre field cannot be empty');
        }else{
            this.primer_nombre.value = removeExtraSpace(this.primer_nombre.value);
            
            if (/[^A-Z ]/.test(this.primer_nombre.value) ) {
                alert('Primer nombre Caracteres invalidos:  solo se permiten letras mayusculas sin tildes y tampoco se permiten letras con caracteres especiales');
                return false;
             }
        }        
        
        if (this.otros_nombres.value || 
            this.otros_nombres.value.trim().length !== 0) {
                
            this.otros_nombres.value = removeExtraSpace(this.otros_nombres.value);
            
            if (/[^A-Z ]/.test(this.otros_nombres.value) ) {
                alert('Otros nombres Caracteres invalidos: solo se permiten letras mayusculas sin tildes y tampoco se permiten letras con caracteres especiales');
                return false;
             }
        }        
    }

    /**
     * 
     * Send parameters to do the register empleado request and clear fields in the view
     */

    handleRegister(event) {
        event.preventDefault();

        this.handleValidations();
        
        // Metodo pasado por medio de props para acer llamado a la API y registrar
        this.props.registerEmpleado({
            tipo_de_documento_identidad_id: this.tipo_de_documento_identidad_id.value, 
            documento_identidad: this.documento_identidad.value, 
            primer_apellido: this.primer_apellido.value, 
            segundo_apellido: this.segundo_apellido.value, 
            primer_nombre: this.primer_nombre.value, 
            otros_nombres: this.otros_nombres.value,
            correo_electronico: this.correo_electronico.value,
            id_pais_del_empleo: this.id_pais_del_empleo.value,
            id_area: this.id_area.value,
            fecha_ingreso: this.fecha_ingreso.value
        });
        

    }    
 
    /**
     * Evento para capturar primer nombre y cambiar el state
     * para hacer el request a la API y generar automaticamente
     * el correo electronico
     */
    handlePrimerNombre(event) {

        event.preventDefault();
        this.validarEmailYCambiarState({ primer_nombre: event.target.value });
    }
 
    /**
     * Evento para capturar primer apellido y cambiar el state
     * para hacer el request a la API y generar automaticamente
     * el correo electronico
     */
    handlePrimerApellido(event) {

        event.preventDefault();
        this.validarEmailYCambiarState({ primer_apellido: event.target.value });

    }   
 
    /**
     * Evento para capturar abreviacion del pais que va al final del dominio
     * y cambiar el state para hacer el request a la API y generar automaticamente
     * el correo electronico
     */
    handleAbreviacion(event) {
        event.preventDefault();

        var obtener_abreviacion = event.target.text.split('-');
        var obtener_abreviacion = obtener_abreviacion[1].trim();

        this.validarEmailYCambiarState({ abreviacion: obtener_abreviacion });
    }
    
    /**
     * LLamar metodo validarEmailEmpleado que hace request a la API
     * para generar correo automaticamente con los valores del state
     * y continuar el flujo del promise para obtener el correo, cambiar el state
     * y mostrarlo en el campo de texto de html
     * @param {*} datosState 
     */
    validarEmailYCambiarState(datosState) {
        
        /**
         * Cambiar state con uso de callback asincrono para poder ver el u
         * nuevo state
         * https://stackoverflow.com/a/34237449/9655579
         */
        this.setState(
            datosState
        , () => {
            this.props.validarEmailEmpleado({
                primer_apellido: this.state.primer_apellido, 
                primer_nombre: this.state.primer_nombre, 
                abreviacion: this.state.abreviacion
            })
            .then(response => {
                if (response.ok) {
                  return response;
                } else {
                  var error = new Error('Error ' + response.status + ': ' + response.statusText);
                  error.response = response;
                  throw error;
                }
              },
              error => {
                    throw error;
              })
            .then(response => response.json())
            .then(response => { 
                console.log('Correo electronico obtenido', response); 
                //alert('Correo electronico obtenido!\n'+JSON.stringify(response.correo_electronico)); 
                this.setState({
                    correo_electronico: response.correo_electronico
                });
            })
            .catch(error =>  { console.log('Error al obtener correo', error.message); });

        });
    }

    /**
     * Render form with their respective validations
     */
    render(){
        
        if (this.props.countries.isLoading) {
		
            return(
                <Loading />
            );
        }
        else if (this.props.countries.errMess) {
            return(
                <h4>{this.props.errMess}</h4>
            );
        }
        else { 
            return(
                <div className="container">
                    
                <br />
				<div className="row">
					<div className="col">
						<Breadcrumb>
							<BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
							<BreadcrumbItem active>Adicionar empleados</BreadcrumbItem>
						</Breadcrumb>
					</div>
				</div>		
                <br />
                    <div className="row row-content">
                        <div className="col-12">
                            <h4>Adicionar Empleado</h4>
                        </div>
                        <div className="col-12 col-md-9">

                            <Form onSubmit={this.handleRegister}>
                                <FormGroup>
                                    <Label for="tipo_de_documento_identidad_id">Tipo de documento identidad</Label>
                                    <Input type="select" name="tipo_de_documento_identidad_id" 
                                    id="tipo_de_documento_identidad_id" innerRef={(input) => this.tipo_de_documento_identidad_id = input}>
                                    {this.props.tipos_de_documento_identidad.tipos_de_documento_identidad.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="documento_identidad">Documento de identidad</Label>
                                    <Input type="text" id="documento_identidad" name="documento_identidad"
                                        innerRef={(input) => this.documento_identidad = input}
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_apellido">Primer apellido</Label>
                                    <Input type="text" id="primer_apellido" name="primer_apellido"
                                        innerRef={(input) => this.primer_apellido = input}
                                        onChange={this.handlePrimerApellido}
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="segundo_apellido">Segundo apellido</Label>
                                    <Input type="text" id="segundo_apellido" name="segundo_apellido"
                                        innerRef={(input) => this.segundo_apellido = input}
                                        maxLength={20}  />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="primer_nombre">Primer nombre</Label>
                                    <Input type="text" id="primer_nombre" name="primer_nombre"
                                        innerRef={(input) => this.primer_nombre = input}
                                        onChange={this.handlePrimerNombre} 
                                        maxLength={20} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="otros_nombres">Otros nombres</Label>
                                    <Input type="text" id="otros_nombres" name="otros_nombres"
                                        innerRef={(input) => this.otros_nombres = input} 
                                        maxLength={50} />
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="correo_electronico">Correo electronico</Label>
                                    <Input type="email" id="correo_electronico" name="correo_electronico"
                                        innerRef={(input) => this.correo_electronico = input}
                                        value={this.state.correo_electronico} 
                                        maxLength={300} />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_pais_del_empleo">Pais del empleo</Label>
                                    <Input type="select" name="id_pais_del_empleo" 
                                    id="id_pais_del_empleo" innerRef={(input) => this.id_pais_del_empleo = input} 
                                    onClick={this.handleAbreviacion}>
                                    {this.props.countries.countries.map((field, i) => { 
                                        /*{
                                            if (i==0){
                                                this.setState({
                                                    abreviacion: field.abreviacion
                                                });
                                            }
                                        }*/
                                        return(
                                            <>
                                                <option value={field.id}>{field.nombre} - {field.abreviacion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="id_area">Area</Label>
                                    <Input type="select" name="id_area" 
                                    id="id_area" innerRef={(input) => this.id_area = input}>
                                    {this.props.areas.areas.map((field, i) => { 
                                        return(
                                            <>
                                                <option value={field.id}>{field.descripcion}</option>
                                            </>
                                        );
                                    }) }
                                    </Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="fecha_ingreso">Fecha de ingreso</Label>
                                    <Input type="datetime-local" id="fecha_ingreso" name="fecha_ingreso"
                                        innerRef={(input) => this.fecha_ingreso = input}  />
                                </FormGroup>
                                
                                <Button type="submit" value="submit" color="primary">
                                    <span className="fa fa-plus-square">&nbsp;</span>  
                                    Adicionar
                                </Button>
                            </Form>
                        </div>
                    </div>
                </div>
            );
        }
    }

}
