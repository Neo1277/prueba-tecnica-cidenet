import * as ActionTypes from './ActionTypes';

/* Set reducer to handle redux state */
export const Empleados = (state = { 
    isLoading: true,
    errMess: null,
    empleados:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_EMPLEADOS:
            return {...state, isLoading: false, errMess: null, empleados: action.payload};

        case ActionTypes.EMPLEADOS_LOADING:
            return {...state, isLoading: true, errMess: null, empleados: []}

        case ActionTypes.EMPLEADOS_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
            return state;
    }
};