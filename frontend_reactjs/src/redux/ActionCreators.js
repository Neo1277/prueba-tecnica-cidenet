import * as ActionTypes from './ActionTypes';
import { 
  baseUrlApiRest, 
  empleados, 
  countries, 
  areas, 
  tipos_de_documento_identidad,
  validar_email
} from '../shared/baseUrl';

export const fetchEmpleados = (link = null, filtrar_por = null, busqueda = null) => (dispatch) => {

  dispatch(empleadosLoading(true));
  
  /**
   * Validaciones para verficar que los parametros de busqueda
   * y el link con paginacion no esten vacios, si estan vacios
   * hacer el request GET a url sin parametros
   */
  // https://stackoverflow.com/a/61726089/9655579
  if (link==null || link=='') {
    
    link = baseUrlApiRest + empleados;

  } 

  if (typeof filtrar_por != "undefined" && filtrar_por) {
      
      if (typeof busqueda != "undefined" && busqueda) {
        link = link + '?busqueda='+busqueda+'&filtrar_por='+filtrar_por;
      }
  }
  
  return fetch(link, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
  .then(response => {
    if (response.ok) {
      return response;
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  })
  .then(response => response.json())
  .then(empleados => dispatch(addEmpleados(empleados)))
  .catch(error => dispatch(empleadosFailed(error.message)));

}

/* Call action type from empleados reducer */
export const empleadosLoading = () => ({
    type: ActionTypes.EMPLEADOS_LOADING
});

/* Call action type from empleados reducer */
export const empleadosFailed = (errmess) => ({
    type: ActionTypes.EMPLEADOS_FAILED,
    payload: errmess
});

/* Call action type from empleados reducer */
export const addEmpleados = (empleados) => ({
    type: ActionTypes.ADD_EMPLEADOS,
    payload: empleados
});

/**
 * Register Empleado
 */
 export const registerEmpleado = (datosEmpleado) => (dispatch) => {
   
  return fetch(baseUrlApiRest +  empleados, {
      method: "POST",
      body: JSON.stringify(datosEmpleado),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
  })
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          throw error;
    })
  .then(response => response.json())
  .then(response => { 
    console.log('Registro Empleado', response); 
    alert('Empleado registered successfully!\n'); 
    /*window.location.reload(false);*/
    window.location.href = '/home'; 
  })
  .catch(error =>  { 
    console.log('Registro Empleado', error.message); 
    alert('Empleado could not be registered\nError: '+error.message); 
  });
};

/**
 * Edit category
 */
 export const editEmpleado = (datosEmpleado) => (dispatch) => {
  
  var id_tercero = datosEmpleado.id_tercero + '';

  //alert(postId);
  return fetch(baseUrlApiRest +  empleados + '/' + id_tercero, {
      method: "PUT",
      body: JSON.stringify(datosEmpleado),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
  })
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          throw error;
    })
  .then(response => response.json())
  .then(response => { 
    console.log('Edit Empleado', response); 
    alert('Empleado edited successfully!\n'); 
    /*window.location.reload(false);*/
    window.location.href = '/home'; 
  })
  .catch(error =>  { 
    console.log('Edit Empleado', error.message); 
    alert('Empleado could not be edited\nError: '+error.message); 
  });
};

/**
 * Delete genre
 */
 export const deleteEmpleado = (id_tercero) => (dispatch) => {
  
  var id_tercero_string = id_tercero + '';

  //const bearer = 'Bearer ' + localStorage.getItem('token');
  
  return fetch(baseUrlApiRest +  empleados + '/' + id_tercero_string, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
  })
  .then(response => {
      if (response.ok) {
        return response;
      } else {
        var error = new Error('Error ' + response.status + ': ' + response.statusText);
        error.response = response;
        throw error;
      }
    },
    error => {
          throw error;
    })
  .then(response => response.json())
  .then(response => { 
    console.log('Delete Empleado', response); 
    alert('Empleado deleted successfully!\n'); 
    /*window.location.reload(false);*/
    window.location.href = '/home'; 
  })
  .catch(error =>  { 
    console.log('Delete Empleado', error.message); 
    alert('Empleado could not be deleted\nError: '+error.message); 
  });
};


/* Request to Laravel API and show error or proceed to dispatch the data  */
export const fetchCountries = () => (dispatch) => {

  dispatch(countriesLoading(true));

  return fetch(baseUrlApiRest + countries, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
  .then(response => {
    if (response.ok) {
      return response;
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  })
  .then(response => response.json())
  .then(countries => dispatch(addCountries(countries)))
  .catch(error => dispatch(countriesFailed(error.message)));
}

/* Call action type from countries reducer */
export const countriesLoading = () => ({
  type: ActionTypes.COUNTRIES_LOADING
});

/* Call action type from countries reducer */
export const countriesFailed = (errmess) => ({
  type: ActionTypes.COUNTRIES_FAILED,
  payload: errmess
});

/* Call action type from countries reducer */
export const addCountries = (countries) => ({
  type: ActionTypes.ADD_COUNTRIES,
  payload: countries
});

/* Request to Laravel API and show error or proceed to dispatch the data  */
export const fetchAreas = () => (dispatch) => {

  dispatch(areasLoading(true));

  return fetch(baseUrlApiRest + areas, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
  .then(response => {
    if (response.ok) {
      return response;
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  })
  .then(response => response.json())
  .then(areas => dispatch(addAreas(areas)))
  .catch(error => dispatch(areasFailed(error.message)));
}

/* Call action type from areas reducer */
export const areasLoading = () => ({
  type: ActionTypes.AREAS_LOADING
});

/* Call action type from areas reducer */
export const areasFailed = (errmess) => ({
  type: ActionTypes.AREAS_FAILED,
  payload: errmess
});

/* Call action type from areas reducer */
export const addAreas = (areas) => ({
  type: ActionTypes.ADD_AREAS,
  payload: areas
});


/* Request to Laravel API and show error or proceed to dispatch the data  */
export const fetcTiposDeDocumentoIdentidad = () => (dispatch) => {

  dispatch(tiposDeDocumentoIdentidadLoading(true));

  return fetch(baseUrlApiRest + tipos_de_documento_identidad, {
    method: "GET",
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
  .then(response => {
    if (response.ok) {
      return response;
    } else {
      var error = new Error('Error ' + response.status + ': ' + response.statusText);
      error.response = response;
      throw error;
    }
  })
  .then(response => response.json())
  .then(tipos_de_documento_identidad => dispatch(addTiposDeDocumentoIdentidad(tipos_de_documento_identidad)))
  .catch(error => dispatch(tiposDeDocumentoIdentidadFailed(error.message)));
}

/* Call action type from tipos de documento identidad reducer */
export const tiposDeDocumentoIdentidadLoading = () => ({
  type: ActionTypes.TIPOS_DE_DOCUMENTO_IDENTIDAD_LOADING
});

/* Call action type from tipos de documento identidad reducer */
export const tiposDeDocumentoIdentidadFailed = (errmess) => ({
  type: ActionTypes.TIPOS_DE_DOCUMENTO_IDENTIDAD_FAILED,
  payload: errmess
});

/* Call action type from tipos de documento identidad reducer */
export const addTiposDeDocumentoIdentidad = (tipos_de_documento_identidad) => ({
  type: ActionTypes.ADD_TIPOS_DE_DOCUMENTO_IDENTIDAD,
  payload: tipos_de_documento_identidad
});


/**
 * Request para validar correo electronico, la continuacion de los
 * promises se hace dentro de los componentes que llaman a esta funcion
 * debido a que esta funcion retorna un valor y este debe ser capturado
 */
 export const validarEmailEmpleado = (datosEmpleado) => (dispatch) => {
  //console.log(datosEmpleado);
  return fetch(baseUrlApiRest +  validar_email, {
      method: "POST",
      body: JSON.stringify(datosEmpleado),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "same-origin"
  });
};
