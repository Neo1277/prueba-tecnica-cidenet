import * as ActionTypes from './ActionTypes';

/* Set reducer to handle redux state */
export const TiposDeDocumentoIdentidad = (state = { 
    isLoading: true,
    errMess: null,
    tipos_de_documento_identidad:[]}, action) => {
    switch (action.type) {
        case ActionTypes.ADD_TIPOS_DE_DOCUMENTO_IDENTIDAD:
            return {...state, isLoading: false, errMess: null, tipos_de_documento_identidad: action.payload};

        case ActionTypes.TIPOS_DE_DOCUMENTO_IDENTIDAD_LOADING:
            return {...state, isLoading: true, errMess: null, tipos_de_documento_identidad: []}

        case ActionTypes.TIPOS_DE_DOCUMENTO_IDENTIDAD_FAILED:
            return {...state, isLoading: false, errMess: action.payload};

        default:
            return state;
    }
};