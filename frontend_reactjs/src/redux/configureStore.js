import {createStore, combineReducers, applyMiddleware} from 'redux';
import { Empleados } from './empleados';
import { Countries } from './countries';
import { Areas } from './areas';
import { TiposDeDocumentoIdentidad } from './tipos_de_documento_identidad';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

/* Configure store for letting the data be there even if the page is reloaded */
export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            empleados: Empleados,
            countries: Countries,
            areas: Areas,
            tipos_de_documento_identidad: TiposDeDocumentoIdentidad
        }),
        applyMiddleware(thunk, logger)
    );

    return store;
}